function countLetter(letter, sentence) {
    let result = 0;

     // Check if letter is a single character
  if (typeof letter === 'string' && letter.length === 1) {

    // Loop through each character in sentence
    for (let i = 0; i < sentence.length; i++) {
      // If current character matches given letter, increment result
      if (sentence[i] === letter) {
        result++;
      }
    }

    // Return the count of the given letter in the sentence
    return result;

  } else {
    // If letter is not a single character, return undefined
    return undefined;
  }
    
}


function isIsogram(text) {
    // convert all characters to lowercase to disregard casing
  text = text.toLowerCase();
  
  // iterate over each letter in the text
  for(let i = 0; i < text.length; i++) {
    // check if the current letter appears again in the remaining text
    if(text.indexOf(text[i], i + 1) !== -1) {
      // if it does, return false (not an isogram)
      return false;
    }
  }
  
  // if no repeating letters found, return true (is an isogram)
  return true;
}

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    
    let discountedPrice = price * 0.8;
    let roundedPrice = Math.round(price);
    
    if (age < 13) {
        return undefined;
    } else if (age >= 13 && age <= 21 || age >= 65) {
        return String(Math.round(discountedPrice*100)/100);
    } else if (age >= 22 && age <= 64) {
        return String(roundedPrice);
    }
}

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
  let hotCategories = [];
  let categories = [];
  
  // Loop through the items array to get all categories.
  for (let i = 0; i < items.length; i++) {
    categories.push(items[i].category);
  }
  
  // Remove duplicate categories using the Set object.
  let uniqueCategories = new Set(categories);
  
   // Convert the Set object to an array using the spread operator.
   let uniqueCategoriesArray = [...uniqueCategories];
  
   // Loop through the unique categories array and check if there are items with 0 stocks.
    for (let i = 0; i < uniqueCategoriesArray.length; i++) {
    let category = uniqueCategoriesArray[i];
    let stocks = items.filter(item => item.category === category && item.stocks === 0);
    
    // If there are items with 0 stocks, add the category to the hot categories array.
    if (stocks.length > 0) {
      hotCategories.push(category);
    }
  }
  
  return hotCategories;

}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.



let flyingVoters = [];
  
  // Loop through the candidateA array and check if the current voter voted for candidateB.
    for (let i = 0; i < candidateA.length; i++) {
    let voter = candidateA[i];
    if (candidateB.includes(voter)) {
      flyingVoters.push(voter);
    }
  }
  
  return flyingVoters;
}

 
    
module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};